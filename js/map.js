$(function(){

  rendermap = function(){
      map,
      markers = [
        {latLng: [59.17, 18.3], name: 'Stockholm', weburl: '/redbuses/stockholm.html'},
        {latLng: [55.40, 12.35], name: 'Copenhagen', weburl: '/redbuses/copenhagen.html'},
        {latLng: [60.10, 25.0], name: 'Helsinki', weburl: '/redbuses/helsinki.html'},
        {latLng: [59.26, 24.25], name: 'Tallinn', weburl: '/redbuses/tallin.html'},
        {latLng: [56.57, 24.6], name: 'Riga', weburl: '/redbuses/riga.html'},
        {latLng: [59.53, 30.15], name: 'St Petersburg', weburl: '/redbuses/st-petersburg.html'},
        {latLng: [59.57, 10.42], name: 'Oslo', weburl: '/redbuses/oslo.html'},
        {latLng: [58.58, 5.45], name: 'Stavanger', weburl: '/redbuses/stavanger.html'},
        {latLng: [60.23, 5.19], name: 'Bergen', weburl: '/redbuses/bergen.html'},
        {latLng: [62.47, 6.74], name: 'Ålesund', weburl: '/redbuses/alesund.html'},
        {latLng: [62.09, 7.20], name: 'Geiranger', weburl: '/redbuses/geiranger.html'}
      ],
      values = [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11
      ],

  map = new jvm.WorldMap({
    zoomButtons : false,
    container: $('#map'),
    map: 'europe_mill_en',
    backgroundColor: "none",
    zoomOnScroll: false,
    zoomMin: 2.3,
    zoomMax: 6,
    focusOn: {
      x: 0.65,
      y: 0.35,
      scale: 2.3
    },
    regionsSelectable: false,
    markersSelectable: true,
    markers: markers,
    regionStyle: { 
      initial: {
        fill: '#fe973b',
        r: 5
      }
    },
    markerStyle: {
      initial: {
        fill: '#ffffff',
        r: 15
      },
      selected: {
        fill: '#ffffff'
      }
    },
    series: {
      markers: [{
        attribute: 'r',
      }]
    },
    onRegionSelected: function(){
      if (window.localStorage) {
        window.localStorage.setItem(
          'jvectormap-selected-regions',
          JSON.stringify(map.getSelectedRegions())
        );
      }
    },
    onMarkerSelected: function(){
      if (window.localStorage) {
        window.localStorage.setItem(
          'jvectormap-selected-markers',
          JSON.stringify(map.getSelectedMarkers())
        );
      }
    },
    onRegionOver: function(event){
      event.preventDefault();
    },
    onRegionLabelShow: function(event){
      event.preventDefault();
    },
    onMarkerClick: function(index, code){
      loadcontent(markers[code].weburl);
    }
  });
  map.setSelectedRegions( JSON.parse( window.localStorage.getItem('jvectormap-selected-regions') || '[]' ) );
  map.setSelectedMarkers( JSON.parse( window.localStorage.getItem('jvectormap-selected-markers') || '[]' ) );
  }
});