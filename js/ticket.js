var iframeUrl = "https://office.instantticketing.com/pit/webshop/widget";

	function pitWidgetBootPartner(route, locale, calendarType, theme, discount,
			customerData, partner) {
		pitWidgetBootReturnPartner(route, "", locale, calendarType, theme,
				discount, customerData, partner);
	}
	function pitWidgetBoot(route, locale, calendarType, theme, discount) {
		pitWidgetBootReturnPartner(route, "", locale, calendarType, theme,
				discount, "false", "");
	}

	function pitWidgetBootReturn(route, routeReturn, locale, calendarType, theme,
			discount) {
		pitWidgetBootReturnPartner(route, routeReturn, locale, calendarType, theme,
				discount, "false", "");
	}

	function pitWidgetBootReturnPartner(route, routeReturn, locale, calendarType,
			theme, discount, customerData, partner) {
		$('<a>', {
			"class" : "s-w-sep-close",
			"href" : "javascript://",
			"onclick" : "pitWidgetClose ()"
		}).css({
			// "background": "url(img/bg_popup-close.png) 0 0 no-repeat",
			"width" : "35px",
			"height" : "35px",
			"position" : "fixed",
			"left" : "50%",
			"top" : "50%",
			"z-index" : 9999,
			"margin" : "-125px 0 0 335px"
		}).appendTo('body')

		if (routeReturn == "") {
			$(
					'<iframe>',
					{
						"id" : "s-w-iframe",
						"frameborder" : 0,
						"allowtransparency" : "true",
						"height" : "340px",
						"width" : "765px",
						"src" : iframeUrl + "?route=" + route + "&locale=" + locale
								+ "&calendarType=" + calendarType + "&theme="
								+ theme + "&discount=" + discount + "&partner="
								+ partner +"&customerData=" + customerData
					}).css({
				"position" : "fixed",
				"left" : "50%",
				"top" : "50%",
				"margin" : "-150px 0 0 -383px",
				"z-index" : "100"
			}).appendTo('body')
		} else {
			$(
					'<iframe>',
					{
						"id" : "s-w-iframe",
						"frameborder" : 0,
						"allowtransparency" : "true",
						"height" : "340px",
						"width" : "765px",
						"src" : iframeUrl + "?route=" + route + "&routeReturn="
								+ routeReturn + "&locale=" + locale
								+ "&calendarType=" + calendarType + "&theme="
								+ theme + "&discount=" + discount + "&partner="
								+ partner+"&customerData=" + customerData
					}).css({
				"position" : "fixed",
				"left" : "50%",
				"top" : "50%",
				"margin" : "-150px 0 0 -383px",
				"z-index" : "100"
			}).appendTo('body')
		}
		$('iframe').before('<div class="s-w-dyn-shadow"></div>')
		$('.s-w-dyn-shadow').css({
			"background-color" : "#000000",
			"height" : "100%",
			"left" : 0,
			"opacity" : 0.5,
			"position" : "fixed",
			"top" : 0,
			"width" : "100%",
			"z-index" : 99
		})

	}

	function pitWidgetBootBundle(bundlegroup, locale, theme, customerData) {
		$('<a>', {
			"class" : "s-w-sep-close",
			"href" : "javascript://",
			"onclick" : "pitWidgetClose ()"
		}).css({
			// "background": "url(img/bg_popup-close.png) 0 0 no-repeat",
			"width" : "35px",
			"height" : "35px",
			"position" : "fixed",
			"left" : "50%",
			"top" : "50%",
			"z-index" : 9999,
			"margin" : "-125px 0 0 335px"
		}).appendTo('body')

		$(
				'<iframe>',
				{
					"id" : "s-w-iframe",
					"frameborder" : 0,
					"allowtransparency" : "true",
					"height" : "340px",
					"width" : "765px",
					"src" : iframeUrl + "?bundlegroup=" + bundlegroup + "&locale="
							+ locale + "&theme=" + theme+"&customerData=" + customerData
				}).css({
			"position" : "fixed",
			"left" : "50%",
			"top" : "50%",
			"margin" : "-150px 0 0 -383px",
			"z-index" : "100"
		}).appendTo('body')

		$('iframe').before('<div class="s-w-dyn-shadow"></div>')
		$('.s-w-dyn-shadow').css({
			"background-color" : "#000000",
			"height" : "100%",
			"left" : 0,
			"opacity" : 0.5,
			"position" : "fixed",
			"top" : 0,
			"width" : "100%",
			"z-index" : 99
		})

	}
	function pitWidgetClose() {
		$('#s-w-iframe, .s-w-dyn-shadow, .s-w-sep-close').remove()
	}