/*

	Created by Tedy Warsitha (Codeorig.in)
	Email: tedy@codeorig.in

*/

(function($) {
	var animatefact = 0.1,
		scrollfact = 0.5,
		gutter = 20,
		breakpointSmall = 479;
	loadcontent = function(e){
		$(".loader").show();
		var leftOffset = ($(window).width());
		var scrollTime = Math.abs(leftOffset-$(window).scrollLeft()) * scrollfact;
		$(".start").animate({left: "-"+leftOffset}, scrollTime);
		$(".footerelements").animate({left: "-"+leftOffset}, scrollTime);
		$.ajax({
			url: e,
			success: function(response) {
				$(".start").hide();
				$("#map").off().empty();
				$(".loader").hide();
				$('.jvectormap-label').remove();
				$("#content").delay(scrollTime).queue(function (next) {
				    $(this).append(response);
				    $("#content").foundation('section', 'reflow');
					$(window).scrollTop(0);
					init();
					setTimeout(function(){
						reset();
					},0);
					next();
				});
			}
		});
		return false;
	};
	var init = function(){
		$(".loader").hide();
		jQuery.fx.interval = animatefact;
		jQuery.easing.def = "easeInOutQuint";
		$(".section-container .title").click(function(){
			setTimeout(function(){
				reset();
			},0);
		});
		if (!jQuery.browser.mobile){
			$(".magnify").mousemove(function(e){
				var image_object = new Image();
				image_object.src = $(this).find(".overview").attr("src");
				var native_width = image_object.width;
				var native_height = image_object.height;
				var magnify_offset = $(this).offset();
				var mx = e.pageX - magnify_offset.left;
				var my = e.pageY - magnify_offset.top;
				if(mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0)
				{
					$(this).find(".magnified").fadeIn(100);
				}
				else
				{
					$(this).find(".magnified").fadeOut(100);
				}
				if($(this).find(".magnified").is(":visible")){
					var rx = Math.round(mx/$(this).find(".overview").width()*native_width - $(this).find(".magnified").width()/2)*-1;
					var ry = Math.round(my/$(this).find(".overview").height()*native_height - $(this).find(".magnified").height()/2)*-1;
					var bgp = rx + "px " + ry + "px";
					var px = mx - $(this).find(".magnified").width()/2;
					var py = my - $(this).find(".magnified").height()/2;
					$(this).find(".magnified").css({left: px, top: py, backgroundPosition: bgp});
				}
			});
			$(".starburst").click(function(event){
				event.preventDefault();
				return false;
			});
		}
	};
	var reset = function(){
		$(".area:not(.start)").each(function(){
			var topOffset = $("#site-header").outerHeight() + gutter*2;
			if ($(window).height() > ($(this).height() + $("#site-header").outerHeight() + gutter*2)){
				var topOffset = $(window).height()*0.5 - $(this).height()*0.5;
			} else {var topOffset = $("#site-header").outerHeight() + gutter*2;}
			$(this).css({
				"position" : "absolute",
				"width" : "100%",
				"z-index" : 5,
				"top" : topOffset+gutter
			});
		});
		if ($(window).width() < breakpointSmall) {
			$(".responsive.tabs").each(function(){
				$(this).removeClass("tabs");
				$(this).attr("data-section", "accordion");
				$(this).addClass("accordion");
			});
		} else {
			$(".responsive.accordion").each(function(){
				$(this).addClass("tabs");
				$(this).attr("data-section", "tabs");
				$(this).removeClass("accordion");
			});
		}
	};
	$(window).load(function(){
		$(document).foundation();
		init();
		reset();
		$(window).scrollTop(0);
		$(window).scrollLeft(0);
		rendermap();
		$(".return").click(function(event){
			$(".start").show();
			$(".jvectormap-label").remove();
			$("#map").empty();
			rendermap();
			event.preventDefault();
			$(".area").animate({left: $(window).width()}, $(window).width()*scrollfact);
			$(".footerelements").animate({left: 0}, $(window).width()*scrollfact);
			$(window).scrollTop(0);
			$("#content").delay($(window).width()*scrollfact).queue(function (next) {
				$(".area:not(.start)").remove();
			    $(".start").animate({left: 0}, $(window).width()*scrollfact);
				$(window).scrollTop(0);
				init();
				setTimeout(function(){
					reset();
				},0);
				next();
			});
		});
	});
	$(window).resize(function(){
		reset();
		$(window).scrollTop(0);
	});
}(jQuery));