<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
	<meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <meta name="description" content="Explore the city with Red Buses Hop-On Hop-Off Sightseeing buses
Hop-On Hop-Off as much as you like at any of the conveniently located stops around the city.
Plan your own itinerary and discover the city’s must-see attractions and activities at your own pace!">
  <meta name="keywords" content="red, buses, hop-on, hop-off, hop, citytour, sightseeing, tourism, travel" />
  <title>Red Buses Hop-On Hop-Off Sightseeing</title>

  <link REL="SHORTCUT ICON" href="favicon.ico">
  <link rel="stylesheet" href="css/normalize.css" />
  <link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.css" />
  <link rel="stylesheet" href="css/foundation.css" />
  <link rel="stylesheet" href="css/jquery-jvectormap-1.2.2.css" type="text/css" media="screen"/>
  <link rel="stylesheet" href="css/app.css" />
  <script src="js/vendor/custom.modernizr.js"></script>
  <script>document.cookie='resolution='+Math.max(screen.width,screen.height)+("devicePixelRatio" in window ? ","+devicePixelRatio : ",1")+'; path=/';</script>

</head>
<body class="transform">

  <div class="loader">
    <div class="inner">
      <img class="loadlogo" src="img/redbuseslogo.png" />
      <div class="rotator"></div>
      <h1>Loading redbuses.com</h1>  
    </div>
  </div>  

  <header id="site-header" class="site-header">
    <a class="return" href="#" title="Return to Map">
      <div class="site-logo"></div>
    </a> 
    <div class="headerelements hide-for-small">
      <a class="return" href="#" title="Return to Map">
        <span aria-hidden="true" class="icon-logo_hoponhopoff"></span>
      </a> 
    </div>       
  </header>
  <div id="container" class="container">
    <div id="content" class="content">
      <?php require_once("start.html"); ?>
    </div><!-- End Content -->
    <div>
  </div><!-- End Container -->
  <div class="footerelements hide-for-small"></div> 
  <footer id="site-footer" class="hide-for-small">
    <div class="row">
      <div class="large-12 columns">
        <span>Copyright &copy 2013 Red Buses. </span>
      </div>
    </div>        
  </footer>  

  <script>
  document.write('<script src=' +
  ('__proto__' in {} ? 'js/vendor/zepto' : 'js/vendor/jquery') +
  '.js><\/script>')
  </script>
  <script src="js/vendor/jquery.js"></script>
  <script src="js/vendor/jquery.easing.min.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/vendor/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="js/vendor/jquery-jvectormap-europe-mill-en.js"></script>
  <script src="js/vendor/jquery.mobile.min.js"></script>
  <script src="js/app.min.js"></script>
  <script src="js/map.min.js"></script>
  <script src="js/ticket.min.js"></script>
</body>
</html>